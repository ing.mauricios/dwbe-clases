const express = require('express');
const app = express();
const { telefonos, saludo } = require('./models/Telefonos');

app.get('/telefonos', (req, res) => {
    saludo();
    res.json(telefonos);
});

app.get('/mitadTelefonos', (req, res) => {
    telefonos.splice(0, (telefonos.length / 2));
    res.json(telefonos);
});

app.listen(3000, () => {
    console.log('Server escuchando en el puerto 3000');
});