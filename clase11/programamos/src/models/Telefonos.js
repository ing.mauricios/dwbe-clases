const telefonos = [
    {
        marca: "Samsung",
        modelo: "S11",
        gama: "Alta",
        pantalla: "19:9",
        sistema_operativo: "Android",
        precio: 1000
    },
    {
        marca: "Iphone",
        modelo: "12 pro",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "iOs",
        precio: 1500
    },
    {
        marca: "Xiaomi",
        modelo: "Note 10s",
        gama: "Media",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 300
    },
    {
        marca: "LG",
        modelo: "LG el que sea",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 800
    }
];

const nombre = "Mi nombre";

const saludo = () => {
    console.log('Hola mundo');
}

module.exports = { telefonos, saludo };