const express = require('express');
const app = express();
require('dotenv').config();

const PORT = process.env.PORT || 3000;

const lista_usuarios = ["user1", "user2", "user3"];

// GET a la raiz de mi servicio web
app.use(express.static('./src/public'));

// CRUD de usuarios
app.get('/usuarios', (req, res) => {
    res.json(lista_usuarios);
});

app.post('/usuarios', (req, res) => {
    res.json('Post de usuario');
});

app.put('/usuarios', (req, res) => {
    res.json('Put de usuario');
});

app.delete('/usuarios', (req, res) => {
    res.json('Delete de usuario');
});


//CRUD de Productos
app.get('/productos', (req, res) => {
    res.json('Get de productos');
});

app.post('/productos', (req, res) => {
    res.json('Post de usuario');
});

app.put('/productos', (req, res) => {
    res.json('Put de usuario');
});

app.delete('/productos', (req, res) => {
    res.json('Delete de usuario');
});

app.listen(PORT, () => {
    console.log('Escuchando en el puerto ' + PORT);
});
