var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');

var users = [
    {
        email: 'ing.mauricios@gmail.com',
        name: 'Mauricio Sierra',
        address: 'Direccion prueba',
        isAdmin: true
    },
    {
        email: 'email@gmail.com',
        name: 'Usuario prueba',
        address: 'Direccion prueba',
        isAdmin: false
    }
];

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    hello: String
    users: [User]
    user(email: String): User
  }

  type Mutation {
      addUser(email: String!, name: String!, address: String): User
  }

  type User {
    email: String!,
    name: String!,
    address: String,
    isAdmin: Boolean!
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
    hello: () => {
        return 'Hello world!';
    },
    users: () => {
        return users;
    },
    user: ({ email }) => {
        return users.find(u => u.email === email);
    },
    addUser: ({ email, name, address }) => {
        users.push({ email, name, address, isAdmin: false });
        return users[users.length - 1];
    }
};

const loggingMiddleware = (req, res, next) => {
    console.log('ip:', req.ip);
    next();
}

var app = express();
app.use(loggingMiddleware);
app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
}));
app.listen(4000);
console.log('Running a GraphQL API server at http://localhost:4000/graphql');