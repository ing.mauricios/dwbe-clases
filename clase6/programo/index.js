// var texto = "texto de ejemplo"

// texto3 = "esto es un texto 3";

// if (true) {
//     var texto2 = "texto de ejemplo 2"
//     console.log(texto);
// }

// function funcionPrubaVar() {
//     var texto4 = "texto 4";
// }

// console.log(texto2);
// console.log(texto3);
// console.log(texto4);


// ////////////////// LET y CONST


// let texto1 = "Texto1";
// const textoConstante = "Hola texto constante";
// textoConstante = "otro texto";

// if (true) {
//     let texto2 = "Texto2"
//     console.log(texto1);
//     console.log(texto2);
// }

// function funcionPrubaVar2() {
//     var texto4 = "texto 4";
//     console.log(texto4);
// }

// funcionPrubaVar();


function saludar() {
    console.log('Hola');
}

let saludar2 = function () {
    console.log('Hola2');
}

let saludar3 = () => {
    console.log('Hola3');
}

function suma(n1, n2, n3 = 0) {
    console.log(n1 + n2 + n3);
}

let suma2 = function (n1, n2) {
    console.log(n1 + n2);
}

let suma3 = (n1, n2) => {
    const resultado = n1 + n2;
    imprimirResultado(resultado);

    function imprimirResultado(result) {
        console.log(result);
    }
}

let sumaMas2 = (n1, n2) => console.log(n1 + n2);

suma3(1, 5);


document.getElementById('btn').addEventListener('click', () => {
    sumaMas2(10, 4);
});