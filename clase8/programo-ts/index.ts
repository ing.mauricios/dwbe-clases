// let nombre = 'Mauricio';
// let altura = 1.79;
// let peso = 85;

// console.log(nombre);
// console.log(altura);
// console.log(peso);

let nombres = ['Mauricio', 'Michael', 'James'];
nombres.push('Lionel');
nombres.push('Ronaldo');
console.log(nombres);

let numeros = [1, 2, 3, 4, 5];
numeros.push(6);
console.log(numeros);

let combinacion = [1, 'Nombre', 2, 'Apellido'];
combinacion.push(1);
combinacion.push('letras');
// combinacion.push(true);

let mascota = {
    nombre: 'Perro',
    descipcion: 'Canino de compañia'
}

mascota.nombre = "Gato";
mascota.descipcion = "Felino de compañia";

//mascota.edad_promedio = 15;

let variable: (number | string)[] = [];
variable.push('string1');
variable.push(55555555);

let mascotas_detallado: {
    nombre: string,
    descripcion: string,
    edad_maxima_promedio: number
}
