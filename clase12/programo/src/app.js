const basicAuth = require('express-basic-auth');
const express = require('express');

const userRoutes = require('./routes/user.routes');
const myCustomAuthorizer = require('./middlewares/basic-auth.middleware');

const app = express();

app.use(express.json());

app.use((req, res, next) => {
    console.log('Time: ', Date.now());
    //res.status(401).json("usuario y contraseña invalidos"); //Si el usuario no esta autorizado para ver una ruta devolver este status
    next();// Si todo va bien hacer next
});

app.use(basicAuth({ authorizer: myCustomAuthorizer }));

app.use('/users', userRoutes);

app.listen(3000, () => { console.log("server listing on port 3000"); });