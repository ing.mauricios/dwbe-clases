const express = require('express');
const router = express.Router();
const User = require('../models/user.model');

router.get('/', (req, res) => {
    console.log(req.auth);
    res.json(User.findAll());
});

router.post('/', (req, res) => {
    User.createNewUser(req.body);
    res.json('User Created');
});

router.put('/', (req, res) => {
    res.json('Put response');
});

router.delete('/', (req, res) => {
    res.json('Delete response');
});

module.exports = router;